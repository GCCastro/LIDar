from keras.models import Sequential, Model
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import RMSprop

import random
import numpy as np
import matplotlib.pyplot as plt



model = Sequential()
#model = Model(input=[a1, a2], output=[b1, b3, b3, b4])
model.add(Dense(4,init='lecun_uniform',input_shape=(2,)))
model.add(Activation('sigmoid'))
#model.load_weights([[ 2.77764511, -1.78160417,  8.61980629,  2.87358737],[-3.63422775, -9.86892891,  0.80245   , -3.64659977]],[-3.82907009,  0.76240861,  0.49609959, -3.72590995])
model.set_weights([np.array([[ 2.05998015, -1.21572554,  0.37625644,  0.21317172],[ 2.26148582,  2.09616017,  4.13443899,  0.84344459]]), np.array([-5.27032566, -5.0163002 , -5.46912146, -4.83895302])])


rms = RMSprop()
model.compile(loss='mse', optimizer=rms)

lado = 100
epochs = 1000
gamma = 0.1
epsilon = 1
iteracoes = []
pos = []
posstr = []

vermelho = [random.randint(0,lado-1),random.randint(0,lado-1)]
#print("Comida: " + str(vermelho))

for i in range(epochs):
   jogador = [random.randint(0,lado-1),random.randint(0,lado-1)]
   j = 0
   while (jogador!=vermelho):
      if(i==epochs-2):
         pos.append(list(jogador))
      inp=[]
      if (jogador[0] < vermelho[0]):
         inp.append(1)
      elif (jogador[0] == vermelho[0]):
         inp.append(0)
      elif (jogador[0] > vermelho[0]):
         inp.append(-1)
      
      if (jogador[1] < vermelho[1]):
         inp.append(1)
      elif (jogador[1] == vermelho[1]):
         inp.append(0)
      elif (jogador[1] > vermelho[1]):
         inp.append(-1)
      
      outp = model.predict(np.array(inp).reshape(1,2),batch_size=1)
      if (random.random()<epsilon):
         action = np.random.randint(0,4)
      else:
         action = np.argmax(outp)

      if (outp[0][0] == max(outp[0])):
         jogador[0]+=1   #DIREITA
         if (i==epochs-2):
            posstr.append('DIREITA')
      if (outp[0][1] == max(outp[0])):
         jogador[0]-=1    #ESQUERDA
         if (i==epochs-2):
            posstr.append('ESQUERDA')
      if (outp[0][2] == max(outp[0])):
         jogador[1]+=1   #CIMA
         if (i==epochs-2):
            posstr.append('CIMA')
      if (outp[0][3] == max(outp[0])):
         jogador[1]-=1   #BAIXO     
         if (i==epochs-2):
            posstr.append('BAIXO')

      if (jogador[0]>lado):
         jogador[0] -= 1
      if (jogador[0]<0):
         jogador[0] += 1
      if (jogador[1]>lado):
         jogador[1] -= 1
      if (jogador[1]<0):
         jogador[1] += 1


      #print(jogador)
      #print(vermelho)

      new_inp=[]
      if (jogador[0] < vermelho[0]):
         new_inp.append(1)
      elif (jogador[0] == vermelho[0]):
         new_inp.append(0)
      elif (jogador[0] > vermelho[0]):
         new_inp.append(-1)
      
      if (jogador[1] < vermelho[1]):
         new_inp.append(1)
      elif (jogador[1] == vermelho[1]):
         new_inp.append(0)
      elif (jogador[1] > vermelho[1]):
         new_inp.append(-1)

      new_outp = model.predict(np.array(new_inp).reshape(1,2),batch_size=1)

      max_outp = max(new_outp[0])
      d = (vermelho[0]-jogador[0])**2+(vermelho[1]-jogador[1])**2

      if (jogador != vermelho):
         #update = -1 + gamma*max_outp
         update = -1/d + gamma*max_outp
      elif (j>lado*5):
         update = -2 + gamma*max_outp
      else:
         update = lado**4

      y = np.zeros((1,4))
      y[:] = outp[:]
      y[0][action] = update

      model.fit(np.array(inp).reshape(1,2),y,batch_size=1, nb_epoch=1)
      
      j += 1
      print(i)
      print(j)
      if (j>lado*5):
         break
   if (epsilon > 0.05):
      epsilon -= 1/epochs
   iteracoes.append(j)

print(iteracoes)
print(model.get_weights())
print(pos)
print(posstr)
plt.plot(iteracoes)
plt.show()
