import sys, pygame
import math

class Cell():
    def __init__(self,r_i,v_i,pos_i,a,b,color):
        self.pos = pos_i
        self.r = r_i
        self.r_i = r_i
        self.a = a
        self.b = b
        self.v = self.a*self.r**(self.b)
        self.color = color

        self.surface = pygame.Surface([2*self.r,2*self.r])
        self.surface.set_alpha(0)
        self.cell = pygame.draw.circle(self.surface,color,[int(self.r), int(self.r)],int(self.r))
        self.vision_surface = pygame.Surface([6*self.r,6*self.r])
        self.vision_surface.set_alpha(0)
        self.vision = pygame.draw.circle(self.vision_surface,color,[int(self.r),int(self.r)],int(3*self.r))
        self.transp = pygame.Surface([6*self.r,6*self.r])
        self.mass = self.r*self.r/100
        self.s = 0
        
    def set(self):
        self.v = self.a*self.r**(self.b)
        self.mass = self.r*self.r/100

        
    def move(self, posit):
        self.cell = self.cell.move(posit)

    def move_vis(self, posit):
        self.vision = self.vision.move(posit)
        
    def check_walls(self, sizemap):
        if (self.pos[1] < 0):
            self.pos[1] = 0
        if (self.pos[1] + 2*self.r > sizemap[1]):
            self.pos[1] = sizemap[1] - 2*self.r
        if (self.pos[0] < 0):
            self.pos[0] = 0
        if (self.pos[0] + 2*self.r > sizemap[0]):
            self.pos[0] = sizemap[1] - 2*self.r
            
