#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, pygame
from pygame.locals import *
import math
import time
from window import Window
from player import Player
from cell import Cell
from food import Food
from botaction import BotAction
from random import randint



pygame.init()

#GLOBAL
size = width, height = 600,600              #Tamanho da janela
sizemap = widthmap, heightmap = 10000,10000 #Tamanho do mapa

r = 200                                     #Raio inicial da celula
v = 12                                      #Velocidade inicial da celula
pos = widthmap/2,heightmap/2                #Posicao inicial da celula
pos2 = 500, 500
color = 0, 0, 255                           #Cor da celula
color2 = 0, 255, 0


foodnumb = 300                              #No inicial de food
foodr = 20                                  #Raio inicial food
foodcolor = 255, 0, 0                       #Cor da food


zoom = 0.01                                 #Zoom inicial
k = 0.09                                    #Const na velocidade apos split
vsplit = 100
d = 200                                     #Limite de massa min para split
a = 200                                     #Decaimento da velocidade com o tamanho
b = -0.6                                    #Outra constante de decaimento da velocidade

alpha = 0.2                                 #Valores para o Q
gamma = 0.5                                 #USADOS PELO OUTRO MANOOO! TIRAR ISTOOO!
g=0.05

ct = 100                                    #Clock tick

white = 255,255,255
clock = pygame.time.Clock()


#SCREEN
#MAIN

try:
    n_bots = int(raw_input("How many bots should be created?"))
except ValueError:
    print "És retardado ou assim?"
    exit()

arena = Window(size,sizemap,zoom)
arena.draw()
food = Food(foodr,foodcolor)
player_list = []
bot_list = []
botaction_list = []
posn_list = []


maincell = Cell(r,v,pos,a,b,color)
mainplayer = Player(maincell,k,vsplit,1, "Eu!",sizemap)

for i in range(n_bots):
    j = 1 + i
    cell = Cell(r,v,(randint(0,sizemap[0]),randint(0,sizemap[1])),a,b,(randint(0,255),randint(0,255),randint(0,255)))
    bot = Player (cell,k,vsplit,i,"Bot %s"%j,sizemap)
    bot_list.append(bot)    
    player_list.append(bot)

player_list.append(mainplayer)


#cell2 = Cell(r,v,pos2,a,b,color2)

#player2 = Player(cell2,k,vsplit,2, "gafote",sizemap)

#player_list = [mainplayer,player2]

#botaction = BotAction(player2, food, player_list)

for bot in bot_list:
    botaction = BotAction(bot,food,bot_list)
    botaction_list.append(botaction)

arena.set(mainplayer)

mainplayer.draw(arena)
mainplayer.move(mainplayer.cell_list[0].pos,sizemap)

for bot in bot_list:
    bot.draw(arena)
    bot.move(bot.cell_list[0].pos,sizemap)

#player2.move(player2.cell_list[0].pos,sizemap)

i = 0
    
while 1:
    
    if (i == 0):
        t = time.time()
        i = 1
    #player_list = [mainplayer,player2]
    while(len(food.pos)<foodnumb):
        food.generate(widthmap, heightmap)
        
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:            
            sys.exit()
            

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                mainplayer.split(d)

                  
    posw = pygame.mouse.get_pos()
    posn = [posw[0]/arena.scalemapwin + arena.left,posw[1]/arena.scalemapwin + arena.top]
    
    for bot in bot_list:
        botaction_list.append(BotAction(bot,food,player_list))

    for botaction in botaction_list:
        posn_list.append(botaction.getposn())
    
    #print "Posicao player2:"
    #print player2.pos
    #print "Massa player2:"
    #print player2.mass    

    
    arena.surface.fill(white)
    mainplayer.move(posn,sizemap)
    print "Posiçao jogador:"
    print mainplayer.pos
    print "Massa jogador:"
    print mainplayer.mass
    mainplayer.update_time()
    mainplayer.check_cells()
    mainplayer.draw(arena)
    for cell in mainplayer.cell_list:
        cell.r = cell.r*math.sqrt(1-0.00002)
    mainplayer.set()
    mainplayer.draw(arena)
    mainplayer.merge()
    food.check_eat(mainplayer,arena)
    mainplayer.check_eat_player(player_list)


    j = 1
    for bot in bot_list:
        bot.move(posn_list[j-1],sizemap)
        print "Posicao bot%s: "% j 
        print bot.pos
        print "Massa bot%s: "% j
        print bot.mass
        bot.update_time()
        bot.check_cells()
        bot.draw(arena)          
        for cell in bot.cell_list:
            cell.r = cell.r*math.sqrt(1-0.00002)
        bot.set()
        bot.draw(arena)
        bot.merge()
        food.check_eat(bot,arena)
        bot.check_eat_player(player_list)
        j += 1
   
        
    arena.set(mainplayer)

        
    arena.draw_grid()
    arena.draw_food(food)
    arena.draw_leaderboard(player_list)



    for player in player_list:
        for cell in player.cell_list:
            if player.flaginit: #### #### #### ####
                player.draw(arena)                
                player.flaginit = False

            arena.surface.blit(cell.surface,cell.cell)    ### ### ### ###    
  
    pygame.display.flip()

    del botaction_list[:]
    del posn_list[:]
