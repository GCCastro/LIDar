import sys, pygame
from pygame.locals import *
import math
import time
from random import randint

from window import Window
from player import Player
from cell import Cell
from food import Food
from botaction import BotAction
from learn import IntPlayer


pygame.init()

#GLOBAL
size = width, height = 600,600              #Tamanho da janela
sizemap = widthmap, heightmap = 10000,10000 #Tamanho do mapa

r = 200                                     #Raio inicial da celula
v = 12                                      #Velocidade inicial da celula
pos = randint(0,sizemap[0]),randint(0,sizemap[1]) #Posicao inicial da celula
pos2 = randint(0,sizemap[0]),randint(0,sizemap[1])

color = 0, 0, 255                           #Cor da celula
color2 = 0, 255, 0

foodnumb = 800                              #No inicial de food
foodr = 20                                  #Raio inicial food
foodcolor = 255, 0, 0                       #Cor da food


zoom = 0.01                                 #Zoom inicial
k = 0.09                                    #Const na velocidade apos split
vsplit = 100
d = 200                                     #Limite de massa min para split
a = 200                                     #Decaimento da velocidade com o tamanho
b = -0.6                                    #Outra constante de decaimento da velocidade

alpha = 0.2                                 #Valores para o Q
gamma = 0.5                                 #USADOS PELO OUTRO MANOOO! TIRAR ISTOOO!
g=0.05

ct = 100                                    #Clock tick

white = 255,255,255
clock = pygame.time.Clock()


#SCREEN
#MAIN

arena = Window(size,sizemap,zoom)
arena.draw()
food = Food(foodr,foodcolor)


maincell = Cell(r,v,pos,a,b,color)
cell2 = Cell(r,v,pos2,a,b,color2)

mainplayer = Player(maincell,k,vsplit,1, "fagote",sizemap)
player2 = IntPlayer(cell2,k,vsplit,2, "gafote",sizemap,17,16,0.001,1.,10,0.9)

player_list = [mainplayer,player2]

arena.set(mainplayer)
arena.set(player2)

mainplayer.draw(arena)
player2.draw(arena)

mainplayer.move(mainplayer.cell_list[0].pos,sizemap)
player2.move(player2.cell_list[0].pos,sizemap)

i = 0
iteracoes = 0
    
while 1:
    
    if (i == 0):
        t = time.time()
        i = 1
    player_list = [mainplayer,player2]
    while(len(food.pos)<foodnumb):
        food.generate(widthmap, heightmap)
        
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT: sys.exit()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                mainplayer.split(d)

                  
    posw = pygame.mouse.get_pos()
    posn = [posw[0]/arena.scalemapwin + arena.left,posw[1]/arena.scalemapwin + arena.top]
    
    #botaction = BotAction(player2,food,player_list)
    #posn2 = botaction.getposn()

    #print "Posicao player1:"
    #print mainplayer.pos
    #print "Massa player1:"
    #print mainplayer.mass
    
    print "Posicao player2:"
    print player2.pos
    print "Massa player2:"
    print player2.mass    

    
    arena.surface.fill(white)
    mainplayer.move(posn,sizemap)
    player2.do(food, player_list, sizemap,0)
    #player2.move(posn2,sizemap)
    oldmass = player2.mass

    for player in player_list:
        player.update_time()
        player.check_cells()
        player.draw(arena)          
        for cell in player.cell_list:
            cell.r = cell.r*math.sqrt(1-0.00002)
        player.set()
        player.draw(arena)
        player.merge()

        food.check_eat(player,arena)
        player.check_eat_player(player_list)
        

    player2.learn(food,player_list,oldmass)
        
    arena.set(mainplayer)

        
    arena.draw_grid()
    arena.draw_food(food)
    arena.draw_leaderboard(player_list)
    


    for player in player_list:
        for cell in player.cell_list:
            arena.surface.blit(cell.surface,cell.cell)    
    pygame.display.flip()

    if not player2.cell_list:
        exit()

    iteracoes += 1
    print("iteracoes:")
    print(iteracoes)
    if (iteracoes == 10000):
        iteracoes = 0
        for p in player_list:
            p.init()
