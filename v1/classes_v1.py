import random
import math

class Cell:
   def __init__(self,x_in,y_in,stat_in):
      self.x = x_in
      self.y = y_in
      self.stat = stat_in

   def check_stat(self):
      return self.stat

   def get_pos(self):
      return [self.x,self.y]

   def change_stat(self,stat_new):
      self.stat = stat_new


class Environment:
   def __init__(self,l_in,b_in,f_in):
      self.l = l_in
      self.b = b_in
      self.f = []
      self.orgs = []
      self.cells = []
      for i in range(0,self.b):
         row=[]
         for j in range(0,self.l):
            c = Cell(i,j,0)
            row.append(c)
         self.cells.append(row)
      self.iteration = 0

   def get_cell(self,x,y):
      return self.cells[x,y]

   def get_cell(self,[x,y]):
      return self.cells[x,y]

   def get_orgs_pos(self)
      l = []
      for o in self.orgs:
         l.append(o.get_pos())
      return l

   def get_adj(self,cell,r):
      l = [x in range(-r-1,r+1)]
      pos = cell.get_pos()
      adj = []
      for i in l:
         for j in l:
            adj.append([pos[0]+i,pos[1]+j])
      return adj

   def filter_cells(self,l):
      for pos in l:
         if (getcell(pos[0],pos[1]).check_stat==2):
            l.remove(pos)

   def define_foodgen(self,pos_x,pos_y):
      self.f.append([pos_x,pos_y])

   def add_orgs(self,orgs_in):
      for l in orgs_in:
         self.orgs.extend(orgs_in)
         self.cells(l.get_pos()[0],l.get_pos([1])).change_stat(2)

   def pos_orgs(self):
      for o in orgs_in:
         self.cells(o.get_pos()[0],o.get_pos([1])).change_stat(2)

   def gen_food(self):
      for g in self.f:
         self.cells[g[0]][g[1]].change_stat(1)
         self.cells[g[0]-1][g[1]].change_stat(1)
         self.cells[g[0]+1][g[1]].change_stat(1)
         self.cells[g[0]][g[1]+1].change_stat(1)
         self.cells[g[0]][g[1]-1].change_stat(1)

   def eat(self):
      l = self.get_orgs_pos()
      for g in self.f:
         for p in l:
            if (g==p):
               o.eat()
               self.cells[g[0]][g[1]].change_stat()

   def draw(self):
      for row in self.cells:
         for c in row:
            print(c.check_stat(),end='')
         print('\n',end='')

   def step(self):
      for o in self.orgs:
         l = self.get_adj(o.get_vision())
         self.filter_cells(l)
         direction = []
         mod = 0
         for c in l:
            if (c.check_stat()==1):
               dr = list(map(operator.sub,c.get_pos(),o.get_pos()))
               mod2 = math.sqrt(dr[0]**2 + dr[1]**2)
               if (mod2 > mod):
                  mod = mod2
                  direction = dr
            direction = [i / mod for i in direction]
         self.get_cell(o.get_pos()).change_stat(0)
         o.move(direction)
         self.eat()
         self.pos_orgs()


class Organism:
   def __init__(self,name_in,v_in,h_in,l_in,pos_in):
      self.name = name_in
      self.vision = v_in
      self.health = h_in
      self.life   = l_in
      self.pos    = pos_in

   def get_vision(self): 
      return self.vision

   def reduce_life(self):
      self.l = self.l - 1
      self.h = self.h - 1

   def get_pos(self):
      return self.pos

   def move(self,dir):
      x = math.copysign(1,dir[0])
      y = math.copysign(1,dir[1])
#      if (x>=0):
#         if (y>=0):
#            l = [0,2,1]
#         else:
#            l = [0,6,7]
#      else:
#         if (y>=0):
#            l = [4,2,3]
#         else:
#            l = [4,6,5]
      if (x>math.abs(math.sqrt(3)/2) && math.abs(y<1/2)):
         c = l[0]
         self.pos = [self.pos[0] + x*1,self.pos[1]]
      elif (x<math.abs(1/2) && y>math.abs(math.sqrt(3)/2)):
         c = l[1]
         self.pos = [self.pos[0],self.pos[1] + y*1]
      else:
         c = l[2]
         self.pos = [self.pos[0] + x*1,self.pos[1] + y*1]     
