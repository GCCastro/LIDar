from pybrain.rl.environments.environment import Environment

class AgariaEnv(Environment):

    def __init__(self, indim, outdim):
        self.indim = indim
        self.outdim = outdim

    def setSensors(self,sensors):
        self.sensors = sensors
        
    def getSensors(self): #Verifica ambiente e obtem inputs
        return self.sensors
        

    def performAction(self, action): #Executa a accao decidida no ambiente
        return action

    
    def reset(self):
        return

