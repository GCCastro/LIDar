import sys, pygame
import math
import time
from window import Window
from player import Player
from cell import Cell
from food import Food
from agariaenv import AgariaEnv
from agariatask import AgariaTask
from pybrain.rl.learners.valuebased import ActionValueTable
from pybrain.rl.learners import Q
from pybrain.rl.explorers import EpsilonGreedyExplorer
from pybrain.rl.experiments import Experiment
from pybrain.rl.agents import LearningAgent
from pybrain.rl.agents.logging import LoggingAgent

pygame.init()

#GLOBAL
size = width, height = 600,600              #Tamanho da janela
sizemap = widthmap, heightmap = 10000,10000 #Tamanho do mapa

r = 200                                     #Raio inicial da celula
v = 12                                      #Velocidade inicial da celula
pos = widthmap/2,heightmap/2                #Posicao inicial da celula
pos2 = 500, 500
color = 0, 0, 255                           #Cor da celula
color2 = 0, 255, 0


foodnumb = 300                              #No inicial de food
foodr = 20                                  #Raio inicial food
foodcolor = 255, 0, 0                       #Cor da food


zoom = 0.01                                 #Zoom inicial
k = 0.09                                    #Const na velocidade apos split
vsplit = 100
d = 200                                     #Limite de massa min para split
a = 200                                     #Decaimento da velocidade com o tamanho
b = -0.6                                    #Outra constante de decaimento da velocidade

alpha = 0.2                                 #Valores para o Q
gamma = 0.5                                 #USADOS PELO OUTRO MANOOO! TIRAR ISTOOO!
g=0.05

ct = 100                                    #Clock tick

white = 255,255,255
clock = pygame.time.Clock()


#MAIN

arena = Window(size,sizemap,zoom)
arena.draw()
food = Food(foodr,foodcolor)


maincell = Cell(r,v,pos,a,b,color)
cell2 = Cell(r,v,pos2,a,b,color2)

mainplayer = Player(maincell,k,vsplit,1)
player2 = Player(cell2,k,vsplit,2)

player_list = [mainplayer,player2]


arena.set(mainplayer)

mainplayer.draw(arena)
player2.draw(arena)

mainplayer.move(mainplayer.cell_list[0].pos,sizemap)
player2.move(player2.cell_list[0].pos,sizemap)


av_table = ActionValueTable(9*2**16, 3)
av_table.initialize(0.)

env = AgariaEnv(9*2**16,3)

task = AgariaTask(env)
task2 = AgariaTask(env)
task_list = [task,task2]

learner = Q(alpha, gamma) ##e' este??
#learner._setExplorer(EpsilonGreedyExplorer(g)) ## e' este??

agent = LoggingAgent(av_table, learner)
experiment = Experiment(task,agent)
agent.integrateObservation(av_table)

food_quantity = []
players_eaten = []
time_reward = [0,0]
i = 0
    
while 1:
    
    if (i == 0):
        t = time.time()
        i = 1
    

    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT: sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                mainplayer.split(d)
                
    sensor = player2.sensor(food,player_list)
    env.setSensors(sensor)
    
    experiment.doInteractions(1)
    agent.learn()
    print env.performaction()

    posw = pygame.mouse.get_pos()
    posn = [posw[0]/arena.scalemapwin + arena.left,posw[1]/arena.scalemapwin + arena.top]
    posn2 = pos2 # mudar para depois ter os resultados do sensor ou assim
    
    arena.surface.fill(white)
    mainplayer.move(posn,sizemap)
    player2.move(posn2,sizemap)    

    for player in player_list:
        player.update_time()
        player.check_cells()
        player.draw(arena)          
        for cell in player.cell_list:
            cell.r = cell.r*math.sqrt(1-0.00002)
        player.set()
        player.draw(arena)
        player.merge()
        food_quantity.append(food.check_eat(player,arena))        
        players_eaten.append(player.check_eat_player(player_list))            


    for player in player_list:
        i = 0
        if (player.time_alive - t > 2):
            time_reward[i] = 1
            t += 2
        i += 1
         
    for x in range(0, len(task_list)):
        task.getReward(food_quantity[x],players_eaten[x],time_reward[x])
        time_reward[x] = 0
        
        
    arena.set(mainplayer)
    while(len(food.pos)<foodnumb):
        food.generate(widthmap, heightmap)
    food.check_eat(player,arena)
        
    arena.draw_grid()
    arena.draw_food(food)
    


    for player in player_list:
        for cell in player.cell_list:
            arena.surface.blit(cell.surface,cell.cell)    
    pygame.display.flip()

    if not player2.cell_list:
        quit
