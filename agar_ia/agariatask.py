from pybrain.rl.environments.task import Task

class AgariaTask(Task):
# task: decide como e' feita a avaliacao das observacoes do meio
# retorna recompensas ou valores de fitness

    def __init__(self, environment):
        self.env = environment # todas as tasks tem de estar associadas a um environment
        self.lastreward = 0 # guardar a reward associada a' ultima interaccao.

    def performAction (self, action):
        self.env.performAction (action) # definido no environment

    def getObservation (self):
        sensors = self.env.getSensors() # idem
        return sensors

    def getReward (self, food, players, time):
        R_alive = time # relacionado com o tempo ha' que esta' vivo
        R_food = food # comeu na ultima accao?
        R_enemy = players # comeu inimigo na ultima accao?
        reward = self.lastreward + R_alive + R_food + R_enemy
        current_reward = self.lastreward
        self.lastreward = reward
        # ordem e' esta pq current reward corresponde a' ultima accao
        
        return current_reward
