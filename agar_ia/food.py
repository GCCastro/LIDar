from random import random
import math

class Food():
    def __init__(self, size, color):
        self.size = size
        self.pos = []
        self.color = color

        
    def generate(self, widthmap, heightmap):
        self.pos.append([int(random()*widthmap),int(random()*heightmap)])

    def check_eat(self, player, arena):
        i = 0
        for cell in player.cell_list:
            for food in self.pos:
                if (math.sqrt((cell.pos[0]+cell.r-food[0])**2+(food[1]-cell.pos[1]-cell.r)**2) <= cell.r):
                    cell.r = math.sqrt(cell.r**2+self.size**2)
                    player.set()
                    player.draw(arena)
                    self.pos.remove(food)
                    i += 1
        return i
            
