import sys, pygame
from pygame.locals import *
import math

class Window():
    def __init__ (self,size,sizemap,zoom):
        self.size = size
        self.sizemap = sizemap
        self.zoom = zoom #percentagem do mapa que eu quero representar
        self.scalemapwin = float(self.size[0])/float(self.sizemap[0])/self.zoom
        self.gridpos = []
        for i in range(self.sizemap[0]/300+1):
            self.gridpos.append(i*300)
        self.center=[0,0]

        
    def set(self, player):
        sumr=0
        summ=0
        sumpos=[0,0]
        n=0
        for cell in player.cell_list:
            n+=1
            sumr+=cell.r
            sumpos[0]+=cell.pos[0]+cell.r
            sumpos[1]+=cell.pos[1]+cell.r
            
        self.center= [sumpos[0]/n,sumpos[1]/n]                 
        self.zoom = 3/5.*(0.2+2/math.pi*0.8*math.atan(0.001*sumr))
        self.scalemapwin = float(self.size[0])/float(self.sizemap[0])/self.zoom
        self.top =int(self.center[1]-self.size[1]/2/self.scalemapwin)
        self.bottom = int(self.center[1]+self.size[1]/2/self.scalemapwin)
        self.left = int(self.center[0]-self.size[0]/2/self.scalemapwin)
        self.right = int(self.center[0]+self.size[0]/2/self.scalemapwin)

        
    def draw(self):
        self.surface = pygame.display.set_mode(self.size,RESIZABLE)
        
    def draw_cell(self,cell,player):
        cell.surface = pygame.Surface([int(2*cell.r*self.scalemapwin),int(2*cell.r*self.scalemapwin)],pygame.SRCALPHA,32)
        cell.cell = pygame.draw.circle(cell.surface,player.color,[int(cell.r*self.scalemapwin), int(cell.r*self.scalemapwin)],int(cell.r*self.scalemapwin))

        pygame.draw.circle(cell.surface,(0,0,0),[int(cell.r*self.scalemapwin), int(cell.r*self.scalemapwin)],int(cell.r*self.scalemapwin),1)
        
#        cell.vision_surface = pygame.Surface([int(6*cell.r*self.scalemapwin),int(6*cell.r*self.scalemapwin)],pygame.SRCALPHA,32)
#        pygame.draw.circle(cell.vision_surface,(0,0,0),[int(cell.r*self.scalemapwin*3), int(cell.r*self.scalemapwin*3)],int(cell.r*self.scalemapwin*3),3)

        cell.move([int((cell.pos[0]-self.left)*self.scalemapwin),int((cell.pos[1]-self.top)*self.scalemapwin)])
       
        # player name
        myfont = pygame.font.SysFont("Ubuntu",14)
        name = myfont.render(player.name, 1, (0,0,0))
        cell.surface.blit(name, name.get_rect(centerx=cell.surface.get_width()/2,centery=cell.surface.get_height()/2))

        
    def draw_food(self,food):            
        for p in food.pos:
            if p[1] in range(self.top, self.bottom) and p[0] in range(self.left, self.right):
                dx = p[0]-self.left
                dy = p[1]-self.top
                pygame.draw.circle(self.surface,food.color,[int(dx*self.scalemapwin),int(dy*self.scalemapwin)],int(food.size*self.scalemapwin))

                
    def draw_grid(self):
        for i in self.gridpos:
            if i in range(self.top, self.bottom):
                dy = i - self.top
                xi = xf = 0
                if self.left < 0 and self.right > self.sizemap[0]:
                    xi =-int(self.left*self.scalemapwin)
                    xf = int((self.sizemap[0]-self.left)*self.scalemapwin)
                elif self.left < 0:
                    xi =-int(self.left*self.scalemapwin)
                    xf =self.size[0]
                elif self.right > self.sizemap[0]:
                    xi = 0
                    xf = int((self.sizemap[0]-self.left)*self.scalemapwin)
                else:
                    xi = 0
                    xf = self.size[0]
                pygame.draw.line(self.surface, (0,0,0) , (xi,int(dy*self.scalemapwin)), (xf, int(dy*self.scalemapwin)),int(3*self.scalemapwin)+1)

            if i in range(self.left, self.right):
                dx = i - self.left
                yi = yf = 0
                if self.top < 0 and self.bottom > self.sizemap[1]:
                    yi =-int((self.top)*self.scalemapwin)
                    yf =int((self.sizemap[1]-self.top)*self.scalemapwin)
                elif self.top < 0:
                    yi =-int((self.top)*self.scalemapwin)
                    yf =self.sizemap[1]
                elif self.bottom > self.sizemap[1]:
                    yi = 0
                    yf = int((self.sizemap[1]-self.top)*self.scalemapwin)
                else:
                    yi = 0
                    yf = self.size[1]
                pygame.draw.line(self.surface, (0,0,0), (int(dx*self.scalemapwin),yi), (int(dx*self.scalemapwin), yf),int(3*self.scalemapwin)+1)

    def draw_leaderboard(self, list):
           myfont = pygame.font.SysFont("Ubuntu",14)
           rect = pygame.Surface((100,200))     # dim rectangulo
           rect.set_alpha(70)                   # transparencia
           rect.fill((23,100,255))              # cor
           self.surface.blit(rect,(2,2))
           title = myfont.render("Leaderboard", 1, (0,0,0))
           self.surface.blit(title, title.get_rect(centerx=rect.get_width()/2, centery = rect.get_height()/20))
           ordered_list = sorted (list,key = lambda player: player.mass, reverse = True)[:10]
           i = 0
           for player in ordered_list:
               jogador = myfont.render(player.name, 1, (0,0,0))
               massa = myfont.render(str(int(player.mass)), 1, (0,0,0))
               self.surface.blit(jogador, jogador.get_rect(centerx=rect.get_width()/2, centery = rect.get_height()/5 + 0.8 * i * rect.get_height()/10))
               #self.surface.blit(massa,(865 - 25*len(str(player.mass)),15+15*i))
               i += 1

