#from player.py import Player
import math

class BotAction():
    def __init__(self,player,food, player_list):
        self.player = player
        self.food = food
        self.player_list = player_list



    def searchfood (self):
        dist = []
        for food_pos in self.food.pos:
            dist.append((math.sqrt((self.player.pos[0]-food_pos[0])**2+(self.player.pos[1]-food_pos[1])**2),self.food.pos.index(food_pos)))
        dmin = 10000
        indexmin = 0
        for d in dist:
            if d[0] < dmin:
                dmin = d[0]
                indexmin = d[1]
            else:
                pass
        x = self.food.pos[indexmin][0]
        y = self.food.pos[indexmin][1]
        return [x,y]
       # angle = math.atan2(y,x)

       # return angle
    
    def detectplayer (self):
        dist = []
        x = 0
        y = 0
        di = 3*sum([ce.r for ce in self.player.cell_list])
        dmin = di
        indexplayer = -1
        indexcell = -1
        for player in self.player_list:
            if player == self.player:
                dist.append((False,self.player_list.index(player),0))
            else:
                for cell in player.cell_list:
                    x = cell.pos[0]-self.player.pos[0]
                    y = cell.pos[1]-self.player.pos[1]
                    d = math.sqrt(x**2+y**2)
                    dist.append((d,self.player_list.index(player),player.cell_list.index(cell)))
        
        for d in dist:
            if (d[0]<di and d[0] != 0):
                indexplayer = d[1]
                indexcell = d[2]
                dmin = d[0]
            else:
                pass
            
        x = int(self.player_list[indexplayer].pos[0])
        y = int(self.player_list[indexplayer].pos[1])
           # angle = math.atan2(y,x)

        
        if dmin == di:
            return [False]
    
        else:
            if self.player_list[indexplayer].cell_list[indexcell].mass > self.player.mass:
                return [True, -1, x, y]
            else:
                return [True, 1, x, y]

        

    def getposn(self):
        if self.detectplayer()[0] == True:
            if self.detectplayer()[1] == 1:
                x = self.detectplayer()[2]
                y = self.detectplayer()[3]
                return [x,y]
            else:
                x = self.player.pos[0]-(self.detectplayer()[2]-self.player.pos[0])
                y = self.player.pos[1]-(self.detectplayer()[3]-self.player.pos[1])
                return [x,y]
        else:
            x = self.searchfood()[0]
            y = self.searchfood()[1]
            return [x,y]
    

                
            
            
            
        
