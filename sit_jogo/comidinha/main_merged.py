import sys , pygame
from pygame.locals import *
import signal
from random import randint
import math
import time
from window import Window
from player import Player
from cell import Cell
from food import Food
from botaction import BotAction
from learn_Q import IntPlayer_Q
import matplotlib.pyplot as plt
from random import uniform


#### #### #### #### #### PLOT #### #### #### #### ####

def signal_handler(signal, frame):
    print(mainplayer.model.get_weights())
    plt.plot(mass_list)
    plt.show()
    sys.exit(0)

try:
    n_bots = int(raw_input("How many bots should be created?  "))
except ValueError:
    print "Not a valid number"
    exit()

try:
    graph = int(raw_input("Graphics (1) or not (0)?  "))
except ValueError:
    print "Not a valid number"
    exit() 

if graph == 1:
    pygame.init()

    
#### #### #### #### #### GLOBAIS #### #### #### #### ####

size = width, height = 600,600              #Tamanho da janela
sizemap = widthmap, heightmap = 10000,10000 #Tamanho do mapa

r = 200                                     #Raio inicial da celula
v = 12                                      #Velocidade inicial da celula
mainpos = sizemap[0]/2, sizemap[1]/2           #Posicao inicial da celula

alpha1 = uniform(0,2*math.pi)
alpha2 = alpha1 + 2*math.pi/16
d1 = uniform(r,3*r)
pos1 = int(mainpos[0]+r+d1*math.cos(alpha1)),int(mainpos[1]+r+d1*math.sin(alpha1))
d1outro = uniform(r,3*r)
pos1outro = int(mainpos[0]+r+d1outro*math.cos(alpha1)),int(mainpos[1]+r+d1outro*math.sin(alpha1))
d2 = uniform(r,3*r)
pos2 = int(mainpos[0]+r+d2*math.cos(alpha2)),int(mainpos[1]+r+d2*math.sin(alpha2))
d2outro = uniform(r,3*r)
pos2outro = int(mainpos[0]+r+d2outro*math.cos(alpha2)),int(mainpos[1]+r+d2outro*math.sin(alpha2))
alpha3 = uniform(0,2*math.pi)
d3 = uniform(r,3*r)
pos3 = int(mainpos[0]+r+d3*math.cos(alpha3)),int(mainpos[1]+r+d3*math.sin(alpha3))
d3outro = uniform(r,3*r)
pos3outro = int(mainpos[0]+r+d3outro*math.cos(alpha3)),int(mainpos[1]+r+d3outro*math.sin(alpha3))


color = 0, 0, 255                           #Cor da celula
color2 =0, 255, 0


foodr = 20                                  #Raio inicial food
foodcolor = 255, 0, 0                       #Cor da food


zoom = 0.01                                 #Zoom inicial
k = 0.09                                    #Const na velocidade apos split
vsplit = 100
d = 200                                     #Limite de massa min para split
a = 200                                     #Decaimento da velocidade com o tamanho
b = -0.6                                    #Outra constante de decaimento da velocidad

ct = 100                                    #Clock tick

white = 255,255,255 
clock = pygame.time.Clock()


#### #### #### #### #### PARAMETROS LEARN #### #### #### #### ####

alpha = 0.1                                 #Valores para o Q
gamma = 0.9                                 
wm = 0.2
wp = 0.8
epsilon=0.2
epochs = 5000
numbrepaction = 40                          #Numero de accoes repetidas
do = numbrepaction
e = 5
foodnumb = 0                              #No inicial de food

if (raw_input("Playing mode? (Y) or (N)") == "Y"):
    print "Not a valid answer"
    epsilon = 0

#### #### #### #### #### MAIN #### #### #### #### ####

if(graph == 1):
    arena = Window(size,sizemap,zoom)
    arena.draw()

    
food = Food(foodr,foodcolor)
food.generate(pos1[0],pos1[1])
food.generate(pos1outro[0],pos1outro[1])
food.generate(pos2[0],pos2[1])
food.generate(pos2outro[0],pos2outro[1])
food.generate(pos3[0],pos3[1])
food.generate(pos3outro[0],pos3outro[1])
player_list = []
posn_list = []


maincell = Cell(r,v,mainpos,a,b,color)
mainplayer = IntPlayer_Q(maincell,k,vsplit,0, "Agar.IA", sizemap,48,16,epsilon,wp,wm,gamma,alpha)
player_list.append(mainplayer)

print(mainplayer.model.get_weights())
if graph == 1:
    arena.set(mainplayer)
    mainplayer.draw(arena)
    
mainplayer.move(mainplayer.cell_list[0].pos,sizemap)

i = 0
iteracoes = 0
episode_size = 1200
j = 0
mass_list = []
re = 0
reward_morte = 0

oldmass = mainplayer.mass

for e in range(epochs):
    #print("epoca:")
    print(e)
    j = 0
    while (iteracoes != 2*do):
        #clock.tick(80)
        if (j == 0):
            oldmass = mainplayer.mass
            j=1
        if (i == 0):
            t = time.time()
            i = 1            
            
        if graph == 1:        
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:            
                    sys.exit()
            arena.surface.fill(white)
            mainplayer.draw(arena)

          
        signal.signal(signal.SIGINT, signal_handler)   
        
        if (do == numbrepaction):
            action = mainplayer.do(food, player_list, sizemap, epochs)
            do = 0
            

        mainplayer.act(action,sizemap)
        do += 1     
        
        for player in player_list:
            player.update_time()
            player.check_cells()
            for cell in player.cell_list: cell.r = cell.r*math.sqrt(1-0.00002)
            player.set()
            player.merge()
    
            food.check_eat(player,0)
            re += player.check_eat_player(player_list)

        if (do == numbrepaction):
            mainplayer.learn(food, player_list, oldmass, re, reward_morte)
            oldmass = mainplayer.mass
            re = 0 
        
        if ((iteracoes != 0) & (mainplayer.flaginit)):
            reward_morte = -100
            mainplayer.learn(food,player_list,oldmass,re,reward_morte)
            reward_morte = 0

        j += 1
        iteracoes += 1

        if graph == 1:
            arena.set(mainplayer)
            arena.draw_grid()
            arena.draw_food(food)
            arena.draw_leaderboard(player_list)

            for player in player_list:
                for cell in player.cell_list:
                    if player.flaginit: #### #### #### ####
                        player.draw(arena)                
                        player.flaginit = False
                    arena.surface.blit(cell.surface,cell.cell)    ### ### ### ###    
            pygame.display.flip()


        
        del posn_list[:]
        player.flaginit = False
    
    mass_list.append(mainplayer.mass)
    iteracoes = 0
    for p in player_list:
        p.init(mainpos)
        alpha1 = uniform(0,2*math.pi)
        alpha2 = alpha1 + 2*math.pi/16
        d1 = uniform(2*r,3*r)
        pos1 = int(mainpos[0]+r+d1*math.cos(alpha1)),int(mainpos[1]+r+d1*math.sin(alpha1))
        d1outro = uniform(2*r,3*r)
        pos1outro = int(mainpos[0]+r+d1outro*math.cos(alpha1)),int(mainpos[1]+r+d1outro*math.sin(alpha1))
        d2 = uniform(2*r,3*r)
        pos2 = int(mainpos[0]+r+d2*math.cos(alpha2)),int(mainpos[1]+r+d2*math.sin(alpha2))
        d2outro = uniform(2*r,3*r)
        pos2outro = int(mainpos[0]+r+d2outro*math.cos(alpha2)),int(mainpos[1]+r+d2outro*math.sin(alpha2))
        alpha3 = uniform(0,2*math.pi)
        d3 = uniform(2*r,3*r)
        pos3 = int(mainpos[0]+r+d3*math.cos(alpha3)),int(mainpos[1]+r+d3*math.sin(alpha3))
        d3outro = uniform(2*r,3*r)
        pos3outro = int(mainpos[0]+r+d3outro*math.cos(alpha3)),int(mainpos[1]+r+d3outro*math.sin(alpha3))
        food = Food(foodr,foodcolor)
        food.generate(pos1[0],pos1[1])
        food.generate(pos1outro[0],pos1outro[1])
        food.generate(pos2[0],pos2[1])
        food.generate(pos2outro[0],pos2outro[1])
        food.generate(pos3[0],pos3[1])
        food.generate(pos3outro[0],pos3outro[1])

        oldmass = p.mass
    j = 0


print(mainplayer.model.get_weights())
plt.plot(mass_list)
plt.show()
