from keras.models import Sequential, Model
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import RMSprop

from player import Player
import numpy as np
import math
import random

class IntPlayer_Q(Player):
    
    def __init__(self,cell,k,vsplit,n,name,sizemap,indim_i, outdim_i, epsilon, wp_i, wm_i, gamma_i,alpha_i):

        Player.__init__(self,cell,k,vsplit,n,name,sizemap)
        self.indim = indim_i
        self.outdim = outdim_i
        self.model = Sequential()
        self.model.add(Dense(self.outdim,init='lecun_uniform',input_shape=(self.indim,)))
        self.model.add(Activation('linear'))

#        self.model.set_weights()

        rms = RMSprop()
        self.model.compile(loss='mse', optimizer=rms)
        self.eps = epsilon
        self.wp = wp_i   #peso de comer jogador
        self.wm = wm_i   #peso da massa
        self.gamma = gamma_i
        self.inp = np.zeros((1,self.indim))
        self.outp = np.zeros((1,self.outdim))
        self.action = []
        self.reward_mass = 0
        self.reward_player = 0
        self.alpha = alpha_i
        self.update = 0

    def do(self,fl,pl,sizemap, epochs):
        oldmass = self.mass
        self.inp = self.sensor(fl,pl)
	self.outp = self.model.predict(np.array(self.inp).reshape(1,self.indim),batch_size=1)
        if self.eps > 0.1:
            self.eps -= 1/epochs
        if (self.outp[0][16]>0):
            spl = True
        else:
            spl = False

        if (random.random()<self.eps):
            self.action = [np.random.randint(0,self.outdim-1),np.random.randint(0,2)]
        else:
            self.action = [np.argmax(self.outp[0]),spl]
        
        return self.action

    def reward(self,oldmass,re,maxQ,Qold,reward_morte):
        self.update = self.alpha * (self.mass - oldmass + reward_morte + re + self.gamma * maxQ - Qold)
        #if ((self.pos[0] <= 500) | (self.pos[0] >= 9500) | (self.pos[1] <= 500) | (self.pos[1] >= 9500)):
        #    self.update -= self.alpha*5

    def learn(self,fl,pl,oldmass,re,reward_morte):

        new_inp = self.sensor(fl,pl)
        new_outp = self.model.predict(np.array(new_inp).reshape(1,self.indim),batch_size=1)

        max_outp = max(new_outp[0])

        self.reward(oldmass,re,max_outp,self.outp[0][self.action[0]],reward_morte)

        y = np.zeros((1,self.outdim))
        y[:] = self.outp[:]
        y[0][self.action[0]] += self.update

        if (self.action[1]):
            y[0][16] += self.update
    
        self.model.fit(np.array(self.inp).reshape(1,self.indim),y,batch_size=1, nb_epoch=1)
        self.update = 0
