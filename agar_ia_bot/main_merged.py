import sys , pygame
from pygame.locals import *
import signal
from random import randint
import math
import time
from window import Window
from player import Player
from cell import Cell
from food import Food
from botaction import BotAction
from learn_Q import IntPlayer_Q
import matplotlib.pyplot as plt


#### #### #### #### #### PLOT #### #### #### #### ####

def signal_handler(signal, frame):
    print(mainplayer.model.get_weights())
    plt.plot(mass_list)
    plt.show()
    sys.exit(0)

try:
    n_bots = int(raw_input("How many bots should be created?  "))
except ValueError:
    print "Es retardado ou assim?"
    exit()

try:
    graph = int(raw_input("Graphics (1) or not (0)?  "))
except ValueError:
    print "Es retardado ou assim?"
    exit() 

if graph == 1:
    pygame.init()

    
#### #### #### #### #### GLOBAIS #### #### #### #### ####

size = width, height = 600,600              #Tamanho da janela
sizemap = widthmap, heightmap = 10000,10000 #Tamanho do mapa

r = 200                                     #Raio inicial da celula
v = 12                                      #Velocidade inicial da celula
pos1 = randint(0,sizemap[0]),randint(0,sizemap[1]) #Posicao inicial da celula
pos2 = randint(0,sizemap[0]),randint(0,sizemap[1])

color = 0, 0, 255                           #Cor da celula
color2 =0, 255, 0


foodr = 20                                  #Raio inicial food
foodcolor = 255, 0, 0                       #Cor da food


zoom = 0.01                                 #Zoom inicial
k = 0.09                                    #Const na velocidade apos split
vsplit = 100
d = 200                                     #Limite de massa min para split
a = 200                                     #Decaimento da velocidade com o tamanho
b = -0.6                                    #Outra constante de decaimento da velocidad

ct = 100                                    #Clock tick

white = 255,255,255 


#### #### #### #### #### PARAMETROS LEARN #### #### #### #### ####

alpha = 0.1                                 #Valores para o Q
gamma = 0.9                                 
wm = 0.2
wp = 0.8
epsilon=0.2
epochs = 1000
numbrepaction = 40                          #Numero de accoes repetidas
do = numbrepaction
e = 5
foodnumb = 500                              #No inicial de food


#### #### #### #### #### MAIN #### #### #### #### ####

if(graph == 1):
    arena = Window(size,sizemap,zoom)
    arena.draw()

    
food = Food(foodr,foodcolor)
player_list = []
bot_list = []
botaction_list = []
posn_list = []

for i in range(n_bots):
    j = 1 + i
    cell = Cell(randint(100,600),v,(randint(0,sizemap[0]),randint(0,sizemap[1])),a,b,(randint(0,255),randint(0,255),randint(0,255)))
    bot = Player (cell,k,vsplit,i,"Bot %s"%j,sizemap)
    bot_list.append(bot)    
    player_list.append(bot)

for bot in bot_list:
    botaction = BotAction(bot,food,bot_list)
    botaction_list.append(botaction)

maincell = Cell(r,v,(randint(0,sizemap[0]),randint(0,sizemap[1])),a,b,color)
mainplayer = IntPlayer_Q(maincell,k,vsplit,0, "fagote", sizemap,48,16,epsilon,wp,wm,gamma,alpha)
player_list.append(mainplayer)

print(mainplayer.model.get_weights())
if graph == 1:
    arena.set(mainplayer)
    mainplayer.draw(arena)
    
mainplayer.move(mainplayer.cell_list[0].pos,sizemap)

for bot in bot_list:
    if graph == 1:
       bot.draw(arena)
    bot.move(bot.cell_list[0].pos,sizemap)

i = 0
iteracoes = 0
episode_size = 1200
j = 0
mass_list = []
re = 0
reward_morte = 0

oldmass = mainplayer.mass

for e in range(epochs):
    #print("epoca:")
    print(e)
    while (iteracoes != 10000):
        if (i == 0):
            t = time.time()
            i = 1
        while(len(food.pos)<foodnumb):
            food.generate(widthmap, heightmap)
                        
        if graph == 1:        
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:            
                    sys.exit()
            arena.surface.fill(white)
            mainplayer.draw(arena)


        for bot in bot_list:
            botaction_list.append(BotAction(bot,food,player_list))

        for botaction in botaction_list:
            posn_list.append(botaction.getposn())

            
        signal.signal(signal.SIGINT, signal_handler)   
        
        
        if (do == numbrepaction):
            action = mainplayer.do(food, player_list, sizemap, epochs)
            if (action[1]):
                mainplayer.split(d)
            do = 0
            

        mainplayer.act(action,sizemap)
        do += 1     
        
        for player in player_list:
            player.update_time()
            player.check_cells()
            for cell in player.cell_list: cell.r = cell.r*math.sqrt(1-0.00002)
            player.set()
            player.merge()
    
            food.check_eat(player,0)
        re += mainplayer.check_eat_player(player_list)

        if (do == numbrepaction):
            mainplayer.learn(food, player_list, oldmass, re, reward_morte)
            oldmass = mainplayer.mass
            re = 0 

        if ((iteracoes != 0) & (mainplayer.flaginit)):
            reward_morte = -100
            mainplayer.learn(food,player_list,oldmass,re,reward_morte)
            reward_morte = 0

        j += 1
        iteracoes += 1

        
        k = 1
        for bot in bot_list:
            bot.move(posn_list[k-1],sizemap)
            bot.update_time()
            bot.check_cells()
            if graph == 1:
                bot.draw(arena)          
            for cell in bot.cell_list:
                cell.r = cell.r*math.sqrt(1-0.00002)
                bot.set()
                if graph == 1:
                    bot.draw(arena)
                bot.merge()
                if graph == 1:
                    food.check_eat(bot,arena)
                else:
                    food.check_eat(bot, 0)
                    
                bot.check_eat_player(player_list)
                k += 1
        if graph == 1:
            arena.set(mainplayer)
            arena.draw_grid()
            arena.draw_food(food)
            arena.draw_leaderboard(player_list)

            for player in player_list:
                for cell in player.cell_list:
                    if player.flaginit: #### #### #### ####
                        player.draw(arena)                
                        player.flaginit = False
                    arena.surface.blit(cell.surface,cell.cell)    ### ### ### ###    
            pygame.display.flip()
        
        del botaction_list[:]
        del posn_list[:]
        player.flaginit = False
    
    mass_list.append(mainplayer.mass)
    iteracoes = 0
    for p in player_list:
        p.init()
        oldmass = p.mass
    j = 0


print(mainplayer.model.get_weights())
plt.plot(mass_list)
plt.show()
