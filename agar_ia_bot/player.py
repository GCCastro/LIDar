import math
from random import randint
from random import choice
import time
from cell import Cell
import copy

d = 200

class Player():
    def __init__(self,cell,k,vsplit,n,name,sizemap):
        self.cell_list_init = [cell]
        self.cell_list = [copy.deepcopy(cell)]
        self.color = cell.color
        self.mass = self.cell_list[0].mass
        self.vsplit = vsplit        
        self.a = cell.a
        self.b = cell.b
        self.k = k
        self.n = n
        self.time_alive = 0
        self.name = name
        self.sizemap = sizemap
        self.flaginit = False
        self.olddist = 0
        
        self.pos = [self.cm()[0]/len(self.cell_list),self.cm()[1]/len(self.cell_list)]

        
    def init (self):
        del self.cell_list[:]
        self.cell_list = copy.deepcopy(self.cell_list_init)
        self.cell_list[0].pos = randint(0,self.sizemap[0]),randint(0,self.sizemap[1])
        self.set()
        self.flaginit = True
    
    def cm (self):
        x = 0
        y = 0
        m = 0
        for cell in self.cell_list:
            x +=cell.pos[0]+cell.r#*cell.mass
            y +=cell.pos[1]+cell.r#*cell.mass
            m +=cell.mass
        return [x, y, m]
            

        
    def move(self, posit, sizemap):
        for cell in self.cell_list:
            x = float(posit[0] - cell.pos[0] - cell.r)
            y = float(posit[1] - cell.pos[1] - cell.r)
            d = math.sqrt(x**2+y**2)
            if d == 0:
                d = 1
            if ((cell.s < 30) & (cell.s > 0)):
                cell.v += self.vsplit*math.exp(-self.k*float(cell.s))
                cell.s += 1
            elif ((cell.s < 100) & (cell.s >29)):
                cell.s += 1
            elif (cell.s >=100):
                cell.s = 0
            cell.pos = [cell.pos[0]+cell.v*x/d,cell.pos[1]+cell.v*y/d]
            cell.check_walls(sizemap)
            

    def set(self):
        self.mass = 0 
        for cell in self.cell_list:
            cell.set()
            self.mass += cell.mass
        self.pos = [self.cm()[0]/len(self.cell_list),self.cm()[1]/len(self.cell_list)]
        


            
    def draw(self, window):
        for cell in self.cell_list:
            window.draw_cell(cell, self)

            
    def check_cells(self):
        for i in range(len(self.cell_list)):
            for j in range(len(self.cell_list)):
                if ((self.cell_list[i].s != 0) | (self.cell_list[j].s != 0)):
                    if (j>i):
                        rs = self.cell_list[i].r+self.cell_list[j].r
                        x = self.cell_list[j].pos[0] - self.cell_list[i].pos[0]
                        y = self.cell_list[j].pos[1] - self.cell_list[i].pos[1]
                        d = math.sqrt(x**2 + y**2)
                        if (d < rs):
                            self.cell_list[j].pos = (self.cell_list[j].pos[0]+x/rs*(rs-d),self.cell_list[j].pos[1]+y/rs*(rs-d))


    def split(self,d):# definir maximo 16 splits e ha um minimo de massa
        for i in range(len(self.cell_list)):
            if self.cell_list[i].mass > d:
                self.cell_list[i].r = self.cell_list[i].r/math.sqrt(2)
                new_cell = Cell(self.cell_list[i].r, self.cell_list[i].v, [self.cell_list[i].pos[0]+20,self.cell_list[i].pos[1]+20],self.a,self.b,self.color)
                new_cell.s = 1
                self.cell_list.append(new_cell)

                
    def merge(self):
        for i in range(len(self.cell_list)):
            for j in range(len(self.cell_list)):
                if ((self.cell_list[i].s == 0) & (self.cell_list[j].s == 0)):
                    if (j>i):
                        rs = max(self.cell_list[i].r,self.cell_list[j].r)
                        x = self.cell_list[j].pos[0]+self.cell_list[j].r - self.cell_list[i].pos[0] - self.cell_list[i].r
                        y = self.cell_list[j].pos[1]+self.cell_list[j].r - self.cell_list[i].pos[1] - self.cell_list[i].r
                        d = math.sqrt(x**2 + y**2) 
                        if (d<rs):
                            self.cell_list[i].mass += self.cell_list[j].mass
                            self.cell_list[i].r = math.sqrt(100*self.cell_list[i].mass)
                            self.cell_list.pop(j)
                            return


    def sensor(self,f,pl):

        no = 16
        nf = 16
        stepang = 2*math.pi/no
        result = [0]*(2*no+nf)
        

        posce = [[ce.pos[0]+ce.r,ce.pos[1]+ce.r] for ce in self.cell_list]
        posc = [sum(c)/len(posce) for c in zip(*posce)]
        ro = 3*sum([ce.r for ce in self.cell_list])
        
        cells = []
        food = []
        numb_food_in_cell = [0]*nf 
        
        for p in pl:
            if (p.n == self.n):
                #pl.pop(p)
                continue
            for ce in p.cell_list:
                cells.append([ce.pos[0]+ce.r,ce.pos[1]+ce.r ,ce.r])
        for pos in f.pos:
                food.append([pos[0]+f.size,pos[1]+f.size])

        for ce in cells:
            x = ce[0]-posc[0] # da' erro se comer o player2
            y = ce[1]-posc[1]
            d = math.sqrt(x**2+y**2)-ce[2]
            if (d<ro):
                angle = math.atan2(y,x) + math.pi
                for n in range(16):                        
                    if angle < (n+1)*stepang:
                        if (self.cell_list[0].r < ce[2]):
                            result[n] = 1
                            break
                        else:
                            result[n+16] = 1
                            break                           
                        
        for pos in food:
            x = pos[0]-posc[0]
            y = pos[1]-posc[1]
            d = math.sqrt(x**2+y**2)
            if (d<ro):
                angle = math.atan2(y,x)+math.pi
                for n in range(16): # as regioes estao numeradas no sentido crescente habitual dos angulos                       
                    if angle < (n+1)*stepang:
                        numb_food_in_cell[n] += 1
			break
                    

        if sum(numb_food_in_cell)!= 0 : 
            for i in range(16) :
                result[i+32] = float(numb_food_in_cell[i])/sum(numb_food_in_cell)
	

        return result

                       
    def act (self, region, sizemap): #este region vai ser membro do player inteligente, em principio
        no = 16 
        angmove= 2*math.pi/no*region[0] + math.pi/no - math.pi
        x = self.pos
        self.move([self.pos[0]+math.cos(angmove), self.pos[1]+math.sin(angmove)],sizemap)
        #if (region[1] > 0): 
            #self.split(d)
        #print("direccao")
        #print([self.pos[0]+math.cos(angmove), self.pos[1]+math.sin(angmove)])
                        
                
                
    def check_eat_player(self,player_list):
        reward = 0
        for selfcell in self.cell_list:
           for player in player_list:
               if player == self:
                   pass
               else:
                   for cell in player.cell_list:
                       dist = math.sqrt((selfcell.pos[0]+selfcell.r-cell.pos[0]-cell.r)**2+(selfcell.pos[1]+selfcell.r-cell.pos[1]-cell.r)**2)
                       if selfcell.mass > 1.2*cell.mass and dist<selfcell.r:
                           selfcell.mass += cell.mass
                           selfcell.r += math.sqrt(100*cell.mass)
                           player.cell_list.remove(cell)## ## ## ##
                           if not player.cell_list: ## ## ## ## 
                               player.init()  
                           reward += cell.mass
                           
        return reward


    def update_time(self):
        self.time_alive = time.time()

    def getwalldist (self):
        return math.sqrt(self.pos[0]**2+self.pos[1]**2)
