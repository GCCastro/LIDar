#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from random import randint
import math
import time
from window import Window
from player import Player
from cell import Cell
from food import Food
from botaction import BotAction



sizemap = widthmap, heightmap = 10000,10000 #Tamanho do mapa

r = 200                                     #Raio inicial da celula
v = 12                                      #Velocidade inicial da celula
pos1 = randint(0,sizemap[0]),randint(0,sizemap[1]) #Posicao inicial da celula
pos2 = randint(0,sizemap[0]),randint(0,sizemap[1])

color = 0, 0, 255                           #Cor da celula
color2 =0, 255, 0


foodnumb = 300                              #No inicial de food
foodr = 20                                  #Raio inicial food
foodcolor = 255, 0, 0                       #Cor da food


#zoom = 0.01                                 #Zoom inicial
k = 0.09                                    #Const na velocidade apos split
vsplit = 100
d = 200                                     #Limite de massa min para split
a = 200                                     #Decaimento da velocidade com o tamanho
b = -0.6                                    #Outra constante de decaimento da velocidade

alpha = 0.2                                 #Valores para o Q
gamma = 0.5                                 #USADOS PELO OUTRO MANOOO! TIRAR ISTOOO!
g=0.05

ct = 100                                    #Clock tick

white = 255,255,255


#MAIN

food = Food(foodr,foodcolor)
player_list = []
botaction_list = []
posn_list = []

try:
    n_players = int(raw_input("How many players should be created?"))
except ValueError:
    print "És retardado ou assim?"
    exit()

for i in range(n_players):
    cell = Cell(r,v,(randint(0,sizemap[0]),randint(0,sizemap[1])),a,b,(randint(0,255),randint(0,255),randint(0,255)))
    player = Player (cell,k,vsplit,i,"i",sizemap)
    player_list.append(player)    

for player in player_list:
    botaction = BotAction(player,food,player_list)
    botaction_list.append(botaction)

#cell1 = Cell(r,v,pos1,a,b,color)
#cell2 = Cell(r,v,pos2,a,b,color2)

#player1 = Player(cell1,k,vsplit,0, "fagote", sizemap)
#player2 = Player(cell2,k,vsplit,1, "gafote", sizemap)

#player_list = [player1,player2]

#botaction1 = BotAction(player1, food, player_list)
#botaction2 = BotAction(player2, food, player_list)




#player1.move(player1.cell_list[0].pos,sizemap)
#player2.move(player2.cell_list[0].pos,sizemap)

i = 0
    
while 1:
    if (i == 0):
        t = time.time()
        i = 1
    #player_list = [player1,player2]
    while(len(food.pos)<foodnumb):
        food.generate(widthmap, heightmap)
        
    
    #botaction1 = BotAction(player1,food,player_list)
    #botaction2 = BotAction(player2,food,player_list)

    for player in player_list:
        botaction_list.append(BotAction(player,food,player_list))

    for botaction in botaction_list:
        posn_list.append(botaction.getposn())    

    j = 0
    for player in player_list:
        player.move(posn_list[j],sizemap)
        print "Posicao player%s: "% j 
        print player.pos
        print "Massa player%s: "% j
        print player.mass
        player.update_time()
        player.check_cells()
        for cell in player.cell_list:
            cell.r = cell.r*math.sqrt(1-0.00002)
        player.set()
        player.merge()
        j += 1
        food.check_eat(player,0)
        player.check_eat_player(player_list)
                         
    #player2.move(posn2,sizemap)
    #print "Posicao player2:"
    #print player2.pos
    #print "Massa player2:"
    #print player2.mass    
        
    del botaction_list[:]
    del posn_list[:]

