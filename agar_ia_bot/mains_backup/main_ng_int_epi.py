import sys
import signal
from random import randint
import math
import time
from window import Window
from player import Player
from cell import Cell
from food import Food
from botaction import BotAction
from learn_epi import IntPlayer_epi
import matplotlib.pyplot as plt

def signal_handler(signal, frame):
    print(player1.model.get_weights())
    plt.plot(mass_list)
    plt.show()
    sys.exit(0)


sizemap = widthmap, heightmap = 10000,10000 #Tamanho do mapa

r = 200                                     #Raio inicial da celula
v = 12                                      #Velocidade inicial da celula
pos1 = randint(0,sizemap[0]),randint(0,sizemap[1]) #Posicao inicial da celula
pos2 = randint(0,sizemap[0]),randint(0,sizemap[1])

color = 0, 0, 255                           #Cor da celula
color2 =0, 255, 0


foodnumb = 300                              #No inicial de food
foodr = 20                                  #Raio inicial food
foodcolor = 255, 0, 0                       #Cor da food


#zoom = 0.01                                 #Zoom inicial
k = 0.09                                    #Const na velocidade apos split
vsplit = 100
d = 200                                     #Limite de massa min para split
a = 200                                     #Decaimento da velocidade com o tamanho
b = -0.6                                    #Outra constante de decaimento da velocidade

alpha = 0.2                                 #Valores para o Q
gamma = 0.5                                 #USADOS PELO OUTRO MANOOO! TIRAR ISTOOO!
g=0.05
epochs = 500

ct = 100                                    #Clock tick

white = 255,255,255


#MAIN

food = Food(foodr,foodcolor)


cell1 = Cell(r,v,pos1,a,b,color)
cell2 = Cell(r,v,pos2,a,b,color2)

player1 = IntPlayer_epi(cell1,k,vsplit,0, "fagote", sizemap,17,16,0.1,0.8,0.2,0.9,0.1)
#player2 = Player(cell2,k,vsplit,1, "gafote", sizemap)
#print(player1.model.get_weights())

player_list = [player1]

#botaction1 = BotAction(player1, food, player_list)
#botaction2 = BotAction(player2, food, player_list)




player1.move(player1.cell_list[0].pos,sizemap)
#player2.move(player2.cell_list[0].pos,sizemap)

i = 0
iteracoes = 0
episode_size = 600
j = 0
mass_list = []
do = 20
re = 0

for e in range(epochs):
    #print("epoca:")
    print(e)
    while (iteracoes != 20000):
        if (i == 0):
            t = time.time()
            i = 1
        player_list = [player1]
        while(len(food.pos)<foodnumb):
            food.generate(widthmap, heightmap)
        signal.signal(signal.SIGINT, signal_handler)   
        
        #botaction1 = BotAction(player1,food,player_list)
        #botaction2 = BotAction(player2,food,player_list)
    
        #posn1 = botaction1.getposn()
        #posn2 = botaction2.getposn()
        
        if (do == 20):
            action = player1.do(food, player_list, sizemap)
            do = 0

        player1.act(action,sizemap)
        do += 1
        
        #player1.move(posn1,sizemap)
        #print "Posicao player1:"
        #print player1.pos
        #print "Massa player1:"
        #print player1.mass
        oldmass = player1.mass
        
        #player2.move(posn2,sizemap)
        #print "Posicao player2:"
        #print player2.pos
        #print "Massa player2:"
        #print player2.mass    
        
        for player in player_list:
            player.update_time()
            player.check_cells()
            for cell in player.cell_list: cell.r = cell.r*math.sqrt(1-0.00002)
            player.set()
            player.merge()
    
            food.check_eat(player,0)
            re += player.check_eat_player(player_list)
            

            if (j == episode_size):
                player1.learn(food,player_list,oldmass,re)
                j = 0
                re = 0
        #print(player1.mass-oldmass)
        j += 1
        iteracoes += 1
        #print(iteracoes)
    mass_list.append(player1.mass)
    iteracoes = 0
    for p in player_list:
        p.init()
        #print("massa nova:")
        #print(p.mass)
        oldmass = p.mass
    j = 0


print(player1.model.get_weights())
plt.plot(mass_list)
plt.show()
