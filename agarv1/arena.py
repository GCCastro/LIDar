import sys, pygame
import math
from random import random
pygame.init()

#CUIDADO ESPILEPIA PERIGO
size = width, height = 600,600
sizemap = widthmap, heightmap = 10000,10000
new_size = new_width, new_height = 600,600
black = 0,0,0
green = 0,255,0
red = 255,0,0
blue = 0,0,255
white = 255,255,255
colors = [white,red,green,blue,black]
vsplit=100
k = 0.09
v = 12
a = 200 #decaimento da velocidade com o tamanho
b = -0.6 #outra constante de decaimento da velocidade
d = 200 #limite de massa min para split
r = 200
clock = pygame.time.Clock()

pos = widthmap/2,heightmap/2

class Window():
    def __init__ (self,size):
        self.size = size
        self.zoom = 0.01 #percentagem do mapa que eu quero representar
        self.scalemapwin = float(self.size[0])/float(sizemap[0])/self.zoom
        self.gridpos = []
        for i in range(sizemap[0]/300+1):
            self.gridpos.append(i*300)
        self.center=[0,0]

    def set(self, player, food):
        sumr=0
        summ=0
        sumpos=[0,0]
        n=0
        for cell in player.cell_list:
            n+=1
            sumr+=cell.r
            sumpos[0]+=cell.pos[0]+cell.r
            sumpos[1]+=cell.pos[1]+cell.r
            
        self.center= [sumpos[0]/n,sumpos[1]/n]                 
        #self.zoom =0.7-30*player.cell_list[0].r**(-0.1) #player.cell_list[0].r**(-0.0007)+0.00008#sera' func,ao da massa/tamanho
        self.zoom = 3/5.*(0.2+2/math.pi*0.8*math.atan(0.001*sumr))
        #print self.zoom
        self.scalemapwin = float(self.size[0])/float(sizemap[0])/self.zoom
        self.top =int(self.center[1]-height/2/self.scalemapwin)
        self.bottom = int(self.center[1]+height/2/self.scalemapwin)
        self.left = int(self.center[0]-width/2/self.scalemapwin)
        self.right = int(self.center[0]+width/2/self.scalemapwin)
    
    def draw(self):
        self.surface = pygame.display.set_mode(self.size)
        
    def draw_cell(self,cell,player):
        cell.surface = pygame.Surface([int(2*cell.r*self.scalemapwin),int(2*cell.r*self.scalemapwin)],pygame.SRCALPHA,32)
        cell.cell = pygame.draw.circle(cell.surface,player.color,[int(cell.r*self.scalemapwin), int(cell.r*self.scalemapwin)],int(cell.r*self.scalemapwin))
        cell.move([int((cell.pos[0]-self.left)*self.scalemapwin),int((cell.pos[1]-self.top)*self.scalemapwin)])
        #player.move([int(width/2-player.cell_list[0].r*self.scalemapwin),int(height/2-player.cell_list[0].r*self.scalemapwin)])

    def draw_food(self,food):            
        for p in food.pos:
            if p[1] in range(self.top, self.bottom) and p[0] in range(self.left, self.right):
                dx = p[0]-self.left
                dy = p[1]-self.top
                pygame.draw.circle(self.surface,colors[int(random()*5)],[int(dx*self.scalemapwin),int(dy*self.scalemapwin)],int(food.size*self.scalemapwin))
    def draw_grid(self):
        for i in self.gridpos:
            if i in range(self.top, self.bottom):
                dy = i - self.top
                xi = xf = 0
                if self.left < 0 and self.right > widthmap:
                    xi =-int(self.left*self.scalemapwin)
                    xf = int((widthmap-self.left)*self.scalemapwin)
                elif self.left < 0:
                    xi =-int(self.left*self.scalemapwin)
                    xf =width
                elif self.right > widthmap:
                    xi = 0
                    xf = int((widthmap-self.left)*self.scalemapwin)
                else:
                    xi = 0
                    xf = width
                pygame.draw.line(self.surface, black, (xi,int(dy*self.scalemapwin)), (xf, int(dy*self.scalemapwin)),int(3*self.scalemapwin)+1)

            if i in range(self.left, self.right):
                dx = i - self.left
                yi = yf = 0
                if self.top < 0 and self.bottom > heightmap:
                    yi =-int((self.top)*self.scalemapwin)
                    yf =int((heightmap-self.top)*self.scalemapwin)
                elif self.top < 0:
                    yi =-int((self.top)*self.scalemapwin)
                    yf =height
                elif self.bottom > heightmap:
                    yi = 0
                    yf = int((heightmap-self.top)*self.scalemapwin)
                else:
                    yi = 0
                    yf = height
                pygame.draw.line(self.surface, black, (int(dx*self.scalemapwin),yi), (int(dx*self.scalemapwin), yf),int(3*self.scalemapwin)+1)

                
class Player():
    def __init__(self,cell):
        self.cell_list = [cell]
        self.color = 255, 100, 100
        self.mass = self.cell_list[0].mass

    def move(self, posit):
        for cell in self.cell_list:
            x = float(posit[0] - cell.pos[0] - cell.r)
            y = float(posit[1] - cell.pos[1] - cell.r)
            d = math.sqrt(x**2+y**2)
            if d == 0:
                d = 1
            if ((cell.s < 30) & (cell.s > 0)):
                cell.v += vsplit*math.exp(-k*float(cell.s))
                cell.s += 1
            elif ((cell.s < 100) & (cell.s >29)):
                cell.s += 1
            elif (cell.s >=100):
                cell.s = 0
            cell.pos = [cell.pos[0]+cell.v*x/d,cell.pos[1]+cell.v*y/d]
            cell.check_walls()

    def set(self):
        for cell in self.cell_list:
            cell.set()
            
    def draw(self, window):
        for cell in self.cell_list:
            window.draw_cell(cell, self)
            
    def check_cells(self):
        for i in range(len(self.cell_list)):
            for j in range(len(self.cell_list)):
                if ((self.cell_list[i].s != 0) | (self.cell_list[j].s != 0)):
                    if (j>i):
                        rs = self.cell_list[i].r+self.cell_list[j].r
                        x = self.cell_list[j].pos[0] - self.cell_list[i].pos[0]
                        y = self.cell_list[j].pos[1] - self.cell_list[i].pos[1]
                        d = math.sqrt(x**2 + y**2)
                        if (d < rs):
                            self.cell_list[j].pos = (self.cell_list[j].pos[0]+x/rs*(rs-d),self.cell_list[j].pos[1]+y/rs*(rs-d))
                            #self.cell_list[j].move((self.cell_list[j].pos[0]+x/r*(r-d),self.cell_list[j].pos[1]+y/r*(r-d)))
                            print (self.cell_list[j].pos[0]+x/r*(r-d),self.cell_list[j].pos[1]+y/r*(r-d))
                

    def split(self):# definir maximo 16 splits e ha um minimo de massa
        for i in range(len(self.cell_list)):
            if self.cell_list[i].mass > d:
                self.cell_list[i].r = self.cell_list[i].r/math.sqrt(2)
                new_cell = Cell(self.cell_list[i].r, self.cell_list[i].v, [self.cell_list[i].pos[0]+20,self.cell_list[i].pos[1]+20])
                new_cell.s = 1
                self.cell_list.append(new_cell)

    def merge(self):
        for i in range(len(self.cell_list)):
            for j in range(len(self.cell_list)):
                if ((self.cell_list[i].s == 0) & (self.cell_list[j].s == 0)):
                    if (j>i):
                        rs = max(self.cell_list[i].r,self.cell_list[j].r)
                        x = self.cell_list[j].pos[0] - self.cell_list[i].pos[0]
                        y = self.cell_list[j].pos[1] - self.cell_list[i].pos[1]
                        d = math.sqrt(x**2 + y**2)
                        print "BATATA d: " + str(d) + " rs: " + str(rs)
                        if (d<rs):
                            print self.cell_list[i].mass
                            self.cell_list[i].mass += self.cell_list[j].mass
                            print self.cell_list[i].mass
                            self.cell_list[i].r = math.sqrt(100*self.cell_list[i].mass)
                            self.cell_list.pop(j)
                            print "JUNTEI!!!!"
                            return
        
        
class Cell():
    def __init__(self,r_i,v_i,pos_i):
        #self.pos = [int(random()*widthmap),int(random()*heightmap)]
        self.pos = pos_i
        self.r = r_i
        self.v = a*self.r**(b)
        self.surface = pygame.Surface([2*self.r,2*self.r])
        self.surface.set_alpha(0)
        self.cell = pygame.draw.circle(self.surface,green,[int(self.r), int(self.r)],int(self.r))
        self.mass = self.r*self.r/100
        self.s = 0

       # print self.mass

    def set(self):
        self.v = a*self.r**(b)
        self.mass = self.r*self.r/100
        print self.mass
        
    def move(self, posit):
        self.cell = self.cell.move(posit)

    def check_walls(self):
        if (self.pos[1] < 0):
            self.pos[1] = 0
        if (self.pos[1] + 2*self.r > heightmap):
            self.pos[1] = heightmap - 2*self.r
        if (self.pos[0] < 0):
            self.pos[0] = 0
        if (self.pos[0] + 2*self.r > widthmap):
            self.pos[0] = widthmap - 2*self.r
            
class Food():
    def __init__(self):
        self.size = 20
        self.pos = []
        self.color = 255,0,0
        
    def generate(self):
        self.pos.append([int(random()*widthmap),int(random()*heightmap)])
        
    def check_eat(self, player, arena):
        for food in self.pos:
            for cell in player.cell_list:
                if (math.sqrt((cell.pos[0]+cell.r-food[0])**2+(food[1]-cell.pos[1]-cell.r)**2) <= cell.r):
            #if(math.fabs(player..cell_list[0]pos[0]-cell[0])<1.41/2*player.cell_list[0].r or math.fabs(player.cell_list[0].pos[1]-cell[1])<1.41/2*player.cell_list[0].r):
                    cell.r = math.sqrt(cell.r**2+self.size**2)
                    player.set()
                    player.draw(arena)
                    #player.move(player.cell_list[0].pos) #porque e' que isto kind of funciona?
                    self.pos.remove(food)

if __name__=="__main__":
    
    arena = Window(size)
    arena.draw()
    food = Food()
    cell = Cell(r,v,pos)
    player = Player(cell)
    arena.set(player,food)
    player.draw(arena)
    player.move(player.cell_list[0].pos)

    while 1:
        clock.tick(100)
        
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    player.split()


        #dis = [-size[i]/2 + pygame.mouse.get_pos()[i] for i in range(2)]
        #disab = math.sqrt(dis[0]**2+dis[1]**2)
        #if (disab == 0):
        #    dis = [0,0]
        #    disab = 1
        posw = pygame.mouse.get_pos()
        posn = [posw[0]/arena.scalemapwin + arena.left,posw[1]/arena.scalemapwin + arena.top]
        player.move(posn)
        player.check_cells()
        player.draw(arena)
        #for cell in player.cell_list:
        #    speed = [int(dis[i]/disab*cell.v) for i in range(2)]
        #    cell.pos = [cell.pos[i] + speed[i] for i in range(2)]
        #    player.cell_list[0].check_walls()

        #    player.draw(arena)
            #player.cell_list[0].check_walls()
            
            #player.cell_list[0].r -= 0.002*player.cell_list[0].mass
        for cell in player.cell_list:
            cell.r = cell.r*math.sqrt(1-0.00002)
        player.set()
        player.draw(arena)
        arena.set(player,food)
        arena.surface.fill(white)
        while(len(food.pos)<300):
            food.generate()
        player.merge()
        food.check_eat(player,arena)
        
        arena.draw_grid()
        arena.draw_food(food)
        for cell in player.cell_list:
            arena.surface.blit(cell.surface,cell.cell) #que e' isto?
        pygame.display.flip() #e isto?


