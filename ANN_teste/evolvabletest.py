from random import random
from pybrain.structure.evolvables.evolvable import Evolvable
from pybrain.optimization import HillClimber

class SimpleEvo(Evolvable):
    def __init__(self, x): self.x = max(0, min(x, 10)) 
    def mutate(self):      self.x = self.x + random() - 0.3
    def copy(self):        return SimpleEvo(self.x)
    def randomize(self):   self.x = 10*random()
    def __repr__(self):    return '<-%.2f->'+str(self.x)
    def printx(self):
        print self.x

x0 = SimpleEvo(9)
l = HillClimber(lambda x:x.x, x0, maxEvaluations = 500, verbose = True, minimize =True) #x.x evaluator
l.learn()




