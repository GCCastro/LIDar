import random
import math
from pybrain.structure import FeedForwardNetwork, LinearLayer, FullConnection, SigmoidLayer
from pybrain.structure.evolvables.evolvable import Evolvable
>>> from pybrain.structure import RecurrentNetwork

if __name__=="__main__":
    lado = 10 #numero de celulas por lado
    passos = [-1,1]

    random.seed()
    vermelho = [random.randint(0,lado-1),random.randint(0,lado-1)]
    jogador = [random.randint(0,lado-1),random.randint(0,lado-1)]
    print("Comida: " + str(vermelho))
    print(jogador)
    
    inputs = [0,0]

    if (vermelho[0]>jogador[0]):
        inputs[0]=1
    elif (vermelho[0]<jogador[0]):
        inputs[0]=-1
    else:
        inputs[0]=0
    if(vermelho[1]>jogador[1]):
        inputs[1]=1
    elif(vermelho[1]<jogador[1]):
        inputs[1]=-1
    else:
        inputs[1]=0
    
    n=FeedForwardNetwork()

    inLayer = LinearLayer(2)
    outLayer = SigmoidLayer(4)

    n.addInputModule(inLayer)
    n.addOutputModule(outLayer)

    in_to_out = FullConnection(inLayer, outLayer)
    n.addConnection(in_to_out)
    n.sortModules()
    
    def objF(x): return math.sqrt((vermelho[0]-jogador[0])**2+(vermelho[1]-jogador[1])**2)

    #while (jogador != vermelho):
    for i in range(2):
        outputs=n.activate(inputs)
        
        print outputs
        if(outputs[0]>outputs[1]):
            passos[0]=1
        else:
            passos[0]=-1
        if(outputs[2]>outputs[3]):
            passos[1]=1
        else:
            passos[1]=-1
        print passos
            
        jogador = [jogador[i]+passos[i] for i in range(2)]
        if (jogador[0]>lado):
            jogador[0] -= 1
        if (jogador[0]<0):
            jogador[0] += 1
        if (jogador[1]>lado):
            jogador[1] -= 1
        if (jogador[1]<0):
            jogador[1] += 1


        #print(jogador)

