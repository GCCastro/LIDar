from pybrain.rl.learners.valuebased import ActionValueTable
from pybrain.rl.experiments import Experiment
from pybrain.rl.agents import LearningAgent
from pybrain.rl.agents.logging import LoggingAgent
from pybrain.rl.environments import Environment, Task
from pybrain.rl.learners import Q
from pybrain.optimization import ExactNES
from pybrain.rl.learners import RWR
from pybrain.rl.explorers import EpsilonGreedyExplorer
import matplotlib.pyplot as plt
from random import random, choice

class Env(Environment):

   def __init__(self,l):
      self.lado = l
      self.vermelho = [2,3]
      self.jogador =  [0,0]
      self.d = (self.vermelho[0]-self.jogador[0])**2+(self.vermelho[1]-self.jogador[1])**2
      self.iterations = 0
      self.allActions = [(1,0),(-1,0),(0,1),(0,-1)]
      self.indim = 4

   def getSensors(self):
      inp=[]
      if (self.jogador[0] < self.vermelho[0]):
         inp.append(1)
      elif (self.jogador[0] == self.vermelho[0]):
         inp.append(0)
      elif (self.jogador[0] > self.vermelho[0]):
         inp.append(-1)

      #if (self.jogador[1] < self.vermelho[1]):
      #   inp.append(1)
      #elif (self.jogador[1] == self.vermelho[1]):
      #   inp.append(0)
      #elif (self.jogador[1] > self.vermelho[1]):
      #   inp.append(-1)
      return inp

   def _moveInDir(self, pos, dir):
      temp = [pos[0]+dir[0],pos[1]+dir[1]]
      return temp

   def performAction(self,action):
      action = choice (list(range(len(self.allActions))))
      
      self.jogador = self._moveInDir(self.jogador,self.allActions[action])
      self.d = (self.vermelho[0]-self.jogador[0])**2+(self.vermelho[1]-self.jogador[1])**2
      #if ((action[0]-action[1])/max(action[0],action[1]) > 0.1):
      #    self.jogador[0] += 1
          #print "Fui para a direita!"
      #elif ((action[1]-action[0])/max(action[0],action[1]) > 0.1):
      #    self.jogador[0] -= 1
          #print "Fui para a esquerda!"
      #if ((action[2]-action[3])/max(action[2],action[3]) > 0.1):
      #    self.jogador[1] += 1
          #print "Fui para cima!"
      #elif ((action[3]-action[2])/max(action[2],action[3]) > 0.1):
      #    self.jogador[1] -= 1
          #print "Fui para baixo!"

      if (self.jogador[0]>self.lado):
         self.jogador[0] -= 1
      if (self.jogador[0]<0):
         self.jogador[0] += 1
      if (self.jogador[1]>self.lado):
         self.jogador[1] -= 1
      if (self.jogador[1]<0):
         self.jogador[1] += 1

      self.iterations += 1
   def reset(self):
      self.vermelho = [2,3]
      self.jogador = [0,0]
      self.d = (self.vermelho[0]-self.jogador[0])**2+(self.vermelho[1]-self.jogador[1])**2
      self.iterations = 0

class Tas(Task):
   def __init__(self,environment):
      self.env = environment

   def performAction(self,action):
      self.env.performAction(action)

   def getObservation(self):
      sensors = self.env.getSensors()
      return sensors

   def getReward(self):
       if (self.env.d == 0):
           return 10
       else:
           return 0

   def indim(self):
      return self.env.indim
      
   def outdim(self):
      return self.env.outdim

env = Env (5)
controller = ActionValueTable(25,4)
controller.initialize(1.)
learner = Q()
agent = LearningAgent(controller, learner)
task = Tas(env)
experiment = Experiment(task, agent)
i = 0
iteracoes = []
media = 0

while (i < 1000):
    experiment.doInteractions(1)
    agent.learn()
    agent.reset()
    if (env.d == 0):
       i += 1
       media += env.iterations
       #print "Iteracoes: %s Passos: %s" % (i, env.iterations)
       if (i % 20):
          iteracoes.append(media/20)
          media = 0
       env.reset()
  
plt.plot(iteracoes)
plt.show()      



    
    
