from pybrain.structure.evolvables.evolvable import Evolvable
from pybrain.optimization import HillClimber
from pybrain.structure import FeedForwardNetwork, LinearLayer, FullConnection, SigmoidLayer
from pybrain.structure.evolvables.evolvable import Evolvable
from pybrain.rl.experiments import Experiment, EpisodicExperiment
from pybrain.rl.agents import LearningAgent
from pybrain.rl.agents.logging import LoggingAgent
from pybrain.rl.environments import Environment, Task
from pybrain.rl.learners import Reinforce, ENAC
from pybrain.optimization import ES
from pybrain.rl.explorers import EpsilonGreedyExplorer, NormalExplorer
from pybrain.optimization import ExactNES
from pybrain.rl.learners import RWR
from pybrain.tools.shortcuts import buildNetwork

import math
import random

class Network:

    def __init__(self,inp_n,outp_n):
        self.n=FeedForwardNetwork()

        self.inLayer = LinearLayer(inp_n)
        self.outLayer = SigmoidLayer(outp_n)
        
        self.n.addInputModule(self.inLayer)
        self.n.addOutputModule(self.outLayer)
        
        self.in_to_out = FullConnection(self.inLayer, self.outLayer)
        self.n.addConnection(self.in_to_out)
        
        self.n.sortModules()

    def run(self,inp):
        outp = self.n.activate(inp)
        return outp

    def change(self):
        print(self.in_to_out.params)
        self.in_to_out.params[0] = 1
        print(self.in_to_out.params)

#class SimpleEvo(Evolvable):
#    def __init__(self, neti): 
#        self.net = net
#    def mutate(self):
#        for p in self.net.in_to_out:
#            p = p + random()*0.01
#
#    def copy(self):
#        return SimpleEvo(self.net)
#
#    def randomize(self):
#        for p in self.net.in_to_out:
#            p = random(-1.,1.)
#
#    def __repr__(self):
#        return '<-%.2f->'+str(self.net)
#
#    def printx(self):
#        print self.net

def busca(network):

    lado = 10 #numero de celulas por lado
    #passos = [-1,1]

    vermelho = [random.randint(0,lado-1),random.randint(0,lado-1)]
    
    jogador = [random.randint(0,lado-1),random.randint(0,lado-1)]
    print("Comida: " + str(vermelho))
    print(jogador)
    
    inp=[]
    if (jogador[0] < vermelho[0]):
       inp.append(1)
    elif (jogador[0] == vermelho[0]):
       inp.append(0)
    elif (jogador[0] > vermelho[0]):
       inp.append(-1)

    if (jogador[1] < vermelho[1]):
       inp.append(1)
    elif (jogador[1] == vermelho[1]):
       inp.append(0)
    elif (jogador[1] > vermelho[1]):
       inp.append(-1)

    outp = network.run(inp)


    #jogador = [jogador[i]+random.choice(passos) for i in range(2)]

    r = random.random()
    if (r<outp[0]):
       jogador[0]+=1   #DIREITA
    r = random.random()
    if (r<outp[0]):
       jogador[0]-=1    #ESQUERDA
    r = random.random()
    if (r<outp[1]):
       jogador[0]+=1   #CIMA
    r = random.random()
    if (r<outp[1]):
       jogador[0]-=1   #BAIXO


    if (jogador[0]>lado):
       jogador[0] -= 1
    if (jogador[0]<0):
       jogador[0] += 1
    if (jogador[1]>lado):
       jogador[1] -= 1
    if (jogador[1]<0):
       jogador[1] += 1
    #for i in range(lado+1):
    #   for j in range(lado+1):
    #      if((i==vermelho[0]) & (j == vermelho[1])):
    #         print(2, end="")
    #      elif((i==jogador[0]) & (j == jogador[1])):
    #         print(1, end="")
    #      else:
    #         print(" ", end="")
    #   print("\n", end="")
    
    print(jogador)

class Env(Environment):

   def __init__(self,l):
      self.lado = l
      #self.vermelho = [random.randint(0,self.lado-1),random.randint(0,self.lado-1)]
      self.vermelho = [2,3]
      #self.jogador = [random.randint(0,self.lado-1),random.randint(0,self.lado-1)]
      self.jogador =  [0,0]
      print(self.jogador)
      print(self.vermelho)
      self.d = (self.vermelho[0]-self.jogador[0])**2+(self.vermelho[1]-self.jogador[1])**2
      self.iterations = 0

   def getSensors(self):
      inp=[]
      if (self.jogador[0] < self.vermelho[0]):
         inp.append(1)
      elif (self.jogador[0] == self.vermelho[0]):
         inp.append(0)
      elif (self.jogador[0] > self.vermelho[0]):
         inp.append(-1)

      if (self.jogador[1] < self.vermelho[1]):
         inp.append(1)
      elif (self.jogador[1] == self.vermelho[1]):
         inp.append(0)
      elif (self.jogador[1] > self.vermelho[1]):
         inp.append(-1)
      return inp

   def performAction(self,action):
      
      print("action:")
      print(action)

      #r = random.random()
      if (action[0]>0.5):
         self.jogador[0]+=1   #DIREITA
      #r = random.random()
      if (action[1]>0.5):
         self.jogador[0]-=1    #ESQUERDA
      #r = random.random()
      if (action[2]>0.5):
         self.jogador[1]+=1   #CIMA
      #r = random.random()
      if (action[3]>0.5):
         self.jogador[1]-=1   #BAIXO


      if (self.jogador[0]>self.lado):
         self.jogador[0] -= 1
      if (self.jogador[0]<0):
         self.jogador[0] += 1
      if (self.jogador[1]>self.lado):
         self.jogador[1] -= 1
      if (self.jogador[1]<0):
         self.jogador[1] += 1

      #print(self.jogador)
      self.iterations += 1

   def reset(self):
      #self.vermelho = [random.randint(0,self.lado-1),random.randint(0,self.lado-1)]
      self.vermelho = [2,3]
      #self.jogador = [random.randint(0,self.lado-1),random.randint(0,self.lado-1)]
      self.jogador = [0,0]
      print(self.jogador)
      print(self.vermelho)
      self.d = (self.vermelho[0]-self.jogador[0])**2+(self.vermelho[1]-self.jogador[1])**2
      self.iterations = 0
     


class Tas(Task):
   def __init__(self,environment):
      self.env = environment

   def performAction(self,action):
      print("action da task")
      print(action)
      self.env.performAction(action)

   def getObservation(self):
      sensors = self.env.getSensors()
      return sensors

   def getReward(self):
      oldd = self.env.d
      newd = (self.env.vermelho[0]-self.env.jogador[0])**2+(self.env.vermelho[1]-self.env.jogador[1])**2
      if (newd<oldd):
         newr = 50
      else:
         newr = -50

      self.env.d = newd

      return newr


random.seed()

net = Network(2,4)

print(net.n)
#net.change()
env = Env(5)
task = Tas(env)
learner = Reinforce()
#learner.explorer=NormalExplorer(4,0.001)
agent = LearningAgent(net.n, learner)
#agent.learning=0
print("BATATA")
print(learner._getExplorer().activate([0,0],[0.25,0.25,0.7,0.6]))
experiment = Experiment(task,agent)
print("estou a entrar em desepero")
print(agent.module.activate([-1,0]))

print(net.n.params)
print(net.run([0,0]))

for i in range(1):
   while(env.jogador!=env.vermelho):
      experiment.doInteractions(1)
      agent.learn()
      if (env.iterations%1000==0):
         print("Estou vivo!")
         print(env.iterations)
         print(env.jogador)
         print(env.vermelho)
   print(env.iterations)
   env.reset()


print(list(net.n.params))


#Eve = SimpleEvo(net)
#l = HillClimber(lambda x:x.x, Eve, maxEvaluations = 500, verbose = True)
#l.learn()


